require "spec_helper"
require "jwt"

describe Api::V1::UsersController do
  let!(:user) { create :user }

  context "when the user does not exist" do

    before do
      binding.pry
      token = JWT.encode({ user: user.id }, nil, 'none', { typ: "JWT" })
      get "/api/v1/users"
    end
  end
end
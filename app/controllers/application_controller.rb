class ApplicationController < ActionController::API
#  protect_from_forgery with: :exception
  rescue_from ActionController::RoutingError do |exception|
    render json: { errors: exception }, status: :not_found
  end
end

class Api::V1::UsersController < ApplicationController
  def create
    user = User.create
    render json: user.to_json
  end
end
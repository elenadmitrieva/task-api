class Api::V1::ImagesController < ApplicationController
  before_action :autentification

  def create
    image = user.images.create(images_params)
    render json: { message: "success", data: image }
  end

  def index
    images = user.images
    render json: images.to_json
  end

  private

  def autentification
    unless user
      raise ActionController::RoutingError.new("Not Found User")
    end
  end

  def user
    @user ||= User.where(api_token: params[:token]).first
  end

  def images_params
    params.permit(:link)
  end
end
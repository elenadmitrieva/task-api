class Image
  include Mongoid::Document
  include Mongoid::Timestamps

  field :link, type: String
  field :width, type: Integer
  field :height, type: Integer

  validates_presence_of :link
  embedded_in :user, inverse_of: :images
  mount_base64_uploader :link, ImageUploader
end

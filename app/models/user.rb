class User
  include Mongoid::Document
  include Mongoid::Timestamps

  field :api_token, type: String
  validates :api_token, uniqueness: true
  embeds_many :images

  before_create :generate_api_token

  def generate_api_token
    begin
      self.api_token = UUID.generate # SecureRandom.hex
    end while User.where(api_token: api_token).any?
  end
end

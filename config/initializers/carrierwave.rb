require 'mongoid/grid_fs'

CarrierWave.configure do |config|
  #config.storage              = :grid_fs
  config.grid_fs_access_url   = "/images"
  #config.grid_fs_host         = 'localhost'
end